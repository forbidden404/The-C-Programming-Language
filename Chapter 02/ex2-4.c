#include <stdio.h>

void squeeze(char s[], char t[]);

int main(int argc, char const *argv[])
{
	printf("Using squeeze function in: \n");
	char *s;
	scanf("%ms", &s);

	printf("Deleting the characters in: \n");
	char *t;
	scanf("%ms", &t);
	
	squeeze(s, t);

	printf("The string is now %s\n", s);

	return 0;
}

void squeeze(char s[], char t[])
{
	int i, j, k;
	int state; 
	
	for (i = j = 0; s[i] != '\0'; i++) {
		for (k = 0; t[k] != '\0'; k++) {
			if (s[i] == t[k]){
				state = 1;
				break;
			} 
		}
		if (state == 1) {
			state = 0;
		} else {
			s[j++] = s[i];
		}
	}


	s[j] = '\0';

}