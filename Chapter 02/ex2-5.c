#include <stdio.h>

int any(char s1[], char s2[]);

int main(int argc, char const *argv[])
{
	printf("Using any function in: \n");
	char *s;
	scanf("%ms", &s);

	printf("Searching the characters in: \n");
	char *t;
	scanf("%ms", &t);
	
	printf("The position is s[%d]\n", any(s, t));

	return 0;
}

int any(char s[], char t[])
{
	int i, j;
	int state; 
	
	for (i = 0; s[i] != '\0'; i++) {
		for (j = 0; t[j] != '\0'; j++) {
			if (s[i] == t[j]){
				return i;
			} 
		}
	}


	return -1;

}