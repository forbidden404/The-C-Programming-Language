int main(int argc, char const *argv[])
{
	char boolean;
	int i;
	const int lim = 10000;
	char s[lim + 1];
	
	for (i = 0, boolean = 1; boolean; ++i) {
		if (i >= lim-1)
			boolean = 0;
		if (boolean != 0) {
			c = getchar();
			if (c == '\n')
				boolean = 0;
			else if (c == EOF)
				boolean = 0;
		} 
		if (boolean)
			s[i] = c;
	} 
	return 0;
}