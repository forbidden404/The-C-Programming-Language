#include <stdio.h>

long long int spow(long int base, int power);

int main(int argc, char const *argv[])
{
	unsigned long int int_range = spow(2, sizeof(int) * 8) - 1;
	unsigned long int char_range = spow(2, sizeof(char) * 8) - 1;
	unsigned long int short_range = spow(2, sizeof(short int) * 8) - 1;
	unsigned long long int long_range = spow(2, sizeof(long int) * 8) - 1;

	printf("Signed Int [%ld,%ld]\n", (int_range/2) - int_range, int_range/2);
	printf("Unsigned Int [%d,%ld]\n", 0, int_range);
	printf("Signed Char [%ld,%ld]\n", (char_range/2) - char_range, char_range/2);
	printf("Unsigned Char [%d,%ld]\n", 0, char_range);
	printf("Signed Short [%ld,%ld]\n", (short_range/2) - short_range, short_range/2);
	printf("Unsigned Short [%d,%ld]\n", 0, short_range);
	printf("Signed Long [%lld,%lld]\n", (long_range/2) - long_range, long_range/2);
	printf("Unsigned Long [%d,%llu]\n", 0, long_range);

	return 0;
}

long long int spow(long int base, int power) 
{
	if (power == 0)
		return 1;
	else if (power == 1)
		return base;
	else {
		int i, tmp = base;
		for (i = 2; i <= power; ++i)
			base *= tmp;
		return base;
	}
}