#include <stdio.h>

int htoi(char s[]);
int lower(int c);

int main(int argc, char const *argv[])
{
	printf("Type hexadecimal number: \n");
	char *s;
	scanf("%ms", &s);
	printf("The decimal number is: %d\n", htoi(s));

	return 0;
}

int htoi(char s[])
{
	int n, i;

	n = 0;
	for (i = 0; (s[i] >= '0' && s[i] <= '9') || (lower(s[i]) >= 'a' && lower(s[i]) <= 'f') || lower(s[i]) == 'x'; ++i) {
		if (lower(s[i]) >= 'a' && lower(s[i]) <= 'f') {
			n = 16 * n + (lower(s[i]) - 'a' + 10);
		} else if (s[i] >= '0' && s[i] <= '9'){
			n = 16 * n + (s[i] - '0');
		}
	}
	return n;
}

int lower(int c)
{
	if (c >= 'A' && c <= 'Z')
		return c + 'a' - 'A';
	else
		return c;
}