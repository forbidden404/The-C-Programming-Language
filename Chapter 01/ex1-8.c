#include <stdio.h>

int main(int argc, char const *argv[])
{
	int c, numLInes, numTabs, numBlanks;

	numLInes = 0;
	numBlanks = 0;
	numTabs = 0;
	while ((c = getchar()) != EOF)
		if (c == '\n')
			++numLInes;
		else if (c == '\t')
			++numTabs;
		else if (c == ' ')
			++numBlanks;

	printf("numLines == %d\n", numLInes);
	printf("numBlanks == %d\n", numBlanks);
	printf("numTabs == %d\n", numTabs);
	return 0;
}