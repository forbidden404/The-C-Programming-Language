#include <stdio.h>

main()
{
	float celsius, fahr;
	int lower, upper, step;

	lower = 0;
	upper = 150;
	step = 10;

	printf("Celsius  |  Fahrenheit\n");
	printf("----------------------\n");

	celsius = lower;
	while (celsius <= upper) {
		fahr = (celsius * 9.0/5.0) + 32.0;
		printf("%3.0f\t\t%6.1f\n", celsius, fahr);
		celsius += step;
	}
}