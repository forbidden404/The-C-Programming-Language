#include <stdio.h>

int main(int argc, char const *argv[])
{
	int  c, i, j, length[20];
	for (i = 0; i < 20; ++i)
		length[i] = 0;
	i = 0;

	while ((c = getchar()) != EOF) {
		if (i == 0) {
			if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'))
				++i;
		} else {
			if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'))
				++i;
			else {
				++length[i];
				i = 0;
			}
		}
	}

	for (i = 1; i < 20; ++i) {
		if (length[i] != 0)
			printf("%2d letters word: ", i);
		
		for (j = 0; j < length[i]; ++j)
			printf("=");
		
		if (length[i] != 0)
			printf("|\n");
	}

	return 0;
}