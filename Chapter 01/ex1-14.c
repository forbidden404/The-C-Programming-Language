#define SIZE 'Z' - 'A'

#include <stdio.h>

int main(int argc, char const *argv[])
{
	int i, j, c;
	int words[SIZE];
	
	for (i = 0; i < SIZE; ++i)
		words[i] = 0;

	while ((c = getchar()) != EOF) {
		if (c >= 'A' && c <= 'Z')
			++words[c - 'A'];
		else if (c >= 'a' && c <= 'z')
			++words[c - 'a'];
	}

	for (i = 0; i < SIZE; ++i) {
		printf("%2c: ", i + 'a');

		for (j = 0; j < words[i]; ++j)
			printf("=");

		printf("|\n");
	}


	return 0;
}