#define IN 1
#define OUT 0

#include <stdio.h>

int main(int argc, char const *argv[])
{
	int c, state;

	c = 0, state = OUT;
	while ((c = getchar()) != EOF) {
		if (c == ' ') {
			if (state == OUT) 
				state = IN;
		}
		else {
			if(state == IN) {
				printf(" %c", c);
				state = OUT;
			} else {
				printf("%c", c);
			}
		}
	}
	return 0;
}